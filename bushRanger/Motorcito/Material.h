#pragma once
#include <d3d9.h>
#pragma comment (lib, "d3d9.lib")
#include "d3dx9.h"
#pragma comment (lib, "d3dx9.lib")
#include <string>
#include "MotorcitoApi.h"
#include"Jueguito.h"
class MOTORCITO_API Material
{
private:
	LPCWSTR _imagen;
	LPDIRECT3DTEXTURE9 tex;
	int _blendi;
	int _wraping;
	int _filtro;
public:
	Material(LPCWSTR seinImage/*, LPDIRECT3DDEVICE9 devP*/);
	~Material();
	LPDIRECT3DTEXTURE9 GetTex();
	LPCWSTR getImageName();
	void SetBlend(/*LPDIRECT3DDEVICE9 devP,*/ int blenderElector);
	void SetFiltro(/*LPDIRECT3DDEVICE9 devP,*/ int filtroElector);
	void SetWrap(/*LPDIRECT3DDEVICE9 devP,*/ int wrapElector);
	void SelectBlend(int blendi);
	void SelectWrap(int wraping);
	void SelectFiltro(int filtro);
	void Offset(D3DXVECTOR2 trans, float rot/*, LPDIRECT3DDEVICE9 devP*/);
	int getBlend();
	int getWrap();
	int getFiltro();
};

