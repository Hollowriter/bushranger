#include "stdafx.h"
#include "Tiler.h"


Tiler::Tiler()
{
}


Tiler::~Tiler()
{
}

void Tiler::SetSize(int m, int l)
{
	a.resize(m);
	for (int i = 0; i < m; i++) 
	{
		a[i].resize(l);
		for (size_t j = 0; j < l; j++)
			a[i][j] = rand() % 2;
	}
	filaTileMap = m;
	columnaTileMap = l;
}
void Tiler::AddPalette(LPDIRECT3DTEXTURE9 aTexture) 
{
	seinTextureMap.push_back(aTexture);
}
void Tiler::SetTile(int m, int l, int value) 
{
	a[m][l] = value;
}
vector<LPDIRECT3DTEXTURE9> Tiler::GetPaletteVector() 
{
	return seinTextureMap;
}
vector<vector<int>> Tiler::GetA() 
{
	return a;
}
int Tiler::GetTile(int m, int l) 
{
	return a[m][l];
}
int Tiler::GetFilas() 
{
	return filaTileMap;
}
int Tiler::GetColumnas() 
{
	return columnaTileMap;
}