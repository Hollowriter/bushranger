#ifndef CONECTOR_H
#define CONECTOR_H
#include "Composite.h"
class MOTORCITO_API Conector
{
public:
	Conector();
	~Conector();
	virtual void LoadContent() = 0;
	virtual void ThyFrames() = 0;
};
#endif

