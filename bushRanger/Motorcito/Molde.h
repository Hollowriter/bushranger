#pragma once
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include"MotorcitoApi.h"
// #include"Jueguito.h"
#include"Meshy.h"
#include"stdafx.h"
#include"Inputs.h"
#include"Vertice.h"
#include"Tiler.h"

class MOTORCITO_API Molde
{
protected:
	Meshy* thyMesh;
	Inputs* teclas;
	D3DXVECTOR3 trasl; 
	D3DXVECTOR3 escal; 
	D3DXVECTOR3 rot;
	D3DTEXTUREADDRESS ta;
	D3DTEXTUREFILTERTYPE filter;
	IDirect3DTexture9* my_texture;
	int clasees;
	int thySelector;
	float laX = 0;
	float num = 0;
	float rotXiii = 0;
	bool horrible;
public:
	Molde();
	Molde(D3DXVECTOR3 tr, D3DXVECTOR3 ro, D3DXVECTOR3 es, int classss);
	~Molde();
	void SetTrasl(D3DXVECTOR3 posicion);
	void SetEscal(D3DXVECTOR3 escalado);
	void SetRot(D3DXVECTOR3 rotacion);
	D3DXVECTOR3 GetTrasl();
	D3DXVECTOR3 GetEscal();
	D3DXVECTOR3 GetRot();
	D3DXMATRIX GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca);
	void SetMesh(Meshy* meshu);
	void SetInputs(Inputs* thoseInputs);
	void SetTexturization(IDirect3DTexture9* g_texture);
	void Wrapping(D3DTEXTUREADDRESS tata, D3DTEXTUREFILTERTYPE filtertilter);
	void BlendingThing(int selector);
	void Draw(LPDIRECT3DDEVICE9 dev);
	virtual void Movimiento();
};

