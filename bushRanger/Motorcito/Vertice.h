#pragma once
#define MYFVF ( D3DFVF_XYZ | D3DFVF_NORMAL | D3DFVF_TEX1)
struct Vertice {
	FLOAT x, y, z;
	FLOAT nx, ny, nz;
	FLOAT tu, tv;
	// DWORD color;
};