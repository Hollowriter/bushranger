#include"stdafx.h"
#include"Composite.h"
Composite::Composite()
{
}
Composite::~Composite()
{
	for (size_t i = 0; i < components.size(); i++) {
		if (components[i] != nullptr){
			components.pop_back();
		}
	}
}
D3DXMATRIX Composite::GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca)
{
	// D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = pos.x;
	transMat._42 = pos.y;
	transMat._43 = pos.z;

	// D3DXMATRIX scaMat;
	D3DXMatrixIdentity(&scaMat);
	scaMat._11 = sca.x;
	scaMat._22 = sca.y;
	scaMat._33 = sca.z;

	D3DXMATRIX rotXMat;
	rotXMat._11 = 1;
	rotXMat._22 = cos(rot.x);
	rotXMat._23 = -sin(rot.x);
	rotXMat._32 = sin(rot.x);
	rotXMat._33 = cos(rot.x);

	D3DXMATRIX rotYMat;
	rotYMat._11 = cos(rot.y);
	rotYMat._13 = sin(rot.y);
	rotYMat._31 = -sin(rot.y);
	rotYMat._33 = cos(rot.y);

	// D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(rot.z);
	rotZMat._12 = sin(rot.z);
	rotZMat._21 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);

	thyMatrix = scaMat * rotZMat * transMat;
	return scaMat * rotZMat * transMat;
}
D3DXMATRIX Composite::GetThyMatrix()
{
	return thyMatrix;
}
/*void Composite::Move(D3DXVECTOR3 trasl, D3DXVECTOR3 escal, D3DXVECTOR3 rot) 
{
	Component::Move(trasl, escal, rot);
	thyMatrix = scaMat * rotZMat * transMat;
	laCajita = BringMesh().Transform(_trasl, _escal, _rot);
	UpdateBoundingBox(_trasl, _escal, _rot);
}*/

BoundingBox Composite::BringMesh()
{
	return laCajita;
}

void Composite::UpdateBoundingBox(D3DXVECTOR3 trasl, D3DXVECTOR3 escal, D3DXVECTOR3 rot) 
{
	for (int i = 0; i < components.size(); i++)
	{
		laCajita.Combine(*components[i]->GetBoundingBox());
	}
	laCajita.Refresh();
	if (GetParent())
	{
		GetParent()->UpdateBoundingBox(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(0, 0, 0));
	}
}
void Composite::Add(Component * component)
{
	components.push_back(component);
	component->SetParent(this);
}
void Composite::Remove(Component * component)
{
	delete component;
}
void Composite::Update()
{
	UpdateComposite();

	for (size_t i = 0; i < components.size(); i++)
		components[i]->Update();
}
void Composite::Rendering() 
{
	Component::Rendering();
	RenderingComposite();

	for (size_t i = 0; i < components.size(); i++) 
		components[i]->Rendering();
}
void Composite::UpdateComposite()
{
}
void Composite::RenderingComposite() 
{
}
