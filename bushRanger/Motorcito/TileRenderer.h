#pragma once
#include "Composite.h"
#include "Tiler.h"
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include "Vertice.h"
class MOTORCITO_API TileRenderer : public Composite
{
private:
	LPDIRECT3DDEVICE9 dev;
	Tiler* seinTiles;
	int offsetX;
	int offsetY;
public:
	TileRenderer(LPDIRECT3DDEVICE9 deving);
	~TileRenderer();
	void SetTiler(Tiler* thyTiles);
	Tiler* GetTiler();
	void RenderingComposite() override;
};

