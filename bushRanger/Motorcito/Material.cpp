#include "stdafx.h"
#include "Material.h"


Material::Material(LPCWSTR seinImage/*, LPDIRECT3DDEVICE9 devP*/)
{
	Jueguito* j = Jueguito::GetInstance();
	_imagen = seinImage;
	D3DXCreateTextureFromFile(j->GetDevice(), getImageName(), &tex);
	//devP->SetTexture(0, tex);
}


Material::~Material()
{
}

LPDIRECT3DTEXTURE9 Material::GetTex() 
{
	return tex;
}

void Material::SetBlend(/*LPDIRECT3DDEVICE9 devP,*/ int blenderElector) 
{
	Jueguito* j = Jueguito::GetInstance();
	switch (blenderElector)
	{
	case 0:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		j->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		j->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		break;
	case 1:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		j->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		j->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
		break;
	case 2:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		j->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		j->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		break;
	default:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	}
}

void Material::SetFiltro(/*LPDIRECT3DDEVICE9 devP,*/ int filtroElector) 
{
	Jueguito* j = Jueguito::GetInstance();
	switch (filtroElector)
	{
	case 1:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC);
		break;
	case 2:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_CONVOLUTIONMONO);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_CONVOLUTIONMONO);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_CONVOLUTIONMONO);
		break;
	case 3:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_FORCE_DWORD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_FORCE_DWORD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_FORCE_DWORD);
		break;
	case 4:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_GAUSSIANQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_GAUSSIANQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_GAUSSIANQUAD);
		break;
	case 5:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
		break;
	case 6:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
		break;
	case 7:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_PYRAMIDALQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_PYRAMIDALQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_PYRAMIDALQUAD);
		break;
	default:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
		break;
	}
}

void Material::SetWrap(/*LPDIRECT3DDEVICE9 devP,*/ int wrapElector) 
{
	Jueguito* j = Jueguito::GetInstance();
	switch (wrapElector)
	{
	default:
		break;
	case 1:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		break;
	case 2:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
		break;
	case 3:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		break;
	case 4:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_FORCE_DWORD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_FORCE_DWORD);
		break;
	case 5:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
		break;
	case 6:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRRORONCE);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRRORONCE);
		break;
	}
}

void Material::SelectBlend(int blendi)
{
	_blendi = blendi;
}

void Material::SelectWrap(int wraping)
{
	_wraping = wraping;
}

void Material::SelectFiltro(int filtro)
{
	_filtro = filtro;
}

void Material::Offset(D3DXVECTOR2 trans, float rot/*, LPDIRECT3DDEVICE9 devP*/) 
{
	Jueguito* j = Jueguito::GetInstance();
	D3DXMATRIX transMat2;
	D3DXMatrixIdentity(&transMat2);
	transMat2._31 = trans.x;
	transMat2._32 = trans.y;
	j->GetDevice()->SetTextureStageState(0, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_COUNT2);


	D3DXMATRIX rotMat2;
	D3DXMatrixIdentity(&rotMat2);
	rotMat2._11 = cos(rot);
	rotMat2._12 = sin(rot);
	rotMat2._21 = -sin(rot);
	rotMat2._22 = cos(rot);
	transMat2 = rotMat2*transMat2;
	j->GetDevice()->SetTransform(D3DTS_TEXTURE0, &transMat2);
}

LPCWSTR Material::getImageName() 
{
	return _imagen;
}

int Material::getBlend()
{
	return _blendi;
}

int Material::getWrap()
{
	return _wraping;
}

int Material::getFiltro()
{
	return _filtro;
}
