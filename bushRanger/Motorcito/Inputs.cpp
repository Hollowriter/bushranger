#include "stdafx.h"
#include "Inputs.h"


Inputs::Inputs()
{
}


Inputs::~Inputs()
{
	KeyDev->Unacquire();
	KeyDev->Release();
	dip->Release();
}

void Inputs::InputInitializer(/*HINSTANCE hInstance, HWND hWnd*/)
{
	Jueguito* j = Jueguito::GetInstance();
	DirectInput8Create(j->GethInstance(),
		DIRECTINPUT_VERSION,
		IID_IDirectInput8,
		(void**)&dip,
		NULL);
	dip->CreateDevice(GUID_SysKeyboard, &KeyDev, NULL);
	KeyDev->SetDataFormat(&c_dfDIKeyboard);
	KeyDev->SetCooperativeLevel(j->GethWnd(), DISCL_FOREGROUND | DISCL_NONEXCLUSIVE);
	KeyDev->Acquire();
}

bool Inputs::Pressing(int thyKey) 
{
	if (keys[thyKey]) // se detiene aca
	{
		return true;
	}
	return false;
}

bool Inputs::JustPressed(int thyKey) 
{
	if (keys[thyKey] && !otherKeys[thyKey]) 
	{
		return true;
	}
	return false;
}

bool Inputs::JustReleased(int thyKey) 
{
	if (!keys[thyKey] && otherKeys[thyKey]) 
	{
		return true;
	}
	return false;
}

void Inputs::update() 
{
	for (int i = 0; i < 256; i++) 
	{
		otherKeys[i] = keys[i];
	}
	KeyDev->GetDeviceState(sizeof(keys), &keys);
}