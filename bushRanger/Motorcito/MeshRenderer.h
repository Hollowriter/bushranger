#pragma once
#include "Composite.h"
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include"MotorcitoApi.h"
#include"Vertice.h"
#include<string>
#include"Meshy.h"
#include"Material.h"
#include"BoundingBox.h"
#include"Camara.h"
#include"SeinTexture.h"
using namespace std;
class MOTORCITO_API MeshRenderer : public Composite
{
protected:
	Meshy* meshito;
	Material* carbono;
	LPDIRECT3DDEVICE9 devP;
	D3DTEXTUREADDRESS ta;
	D3DTEXTUREFILTERTYPE filter;
	LPDIRECT3DTEXTURE9 my_texture;
	SeinTexture* texturizado;
	LPD3DXEFFECT thyEffect;
	float laX = 0;
	float num = 0;
	float rotXiii = 0;
	int thySelector;
public:
	MeshRenderer();
	MeshRenderer(SeinTexture* skinner);
	~MeshRenderer();
	void SetMesh(Meshy* meshu);
	void SetMaterial(Material* hidrogeno);
	void CreateEffect(LPD3DXEFFECT efectus);
	LPD3DXEFFECT GetThyEffect();
	BoundingBox BringMesh();
	Material* GetMaterial();
	Meshy* GetMesh();
	void SetTexturization(LPDIRECT3DTEXTURE9 g_texture);
	virtual void SetTexturizationTwo(LPCWSTR seinTexture);
	virtual void SetTexturizationThree(SeinTexture* thaTextura);
	void SetDevPointer(LPDIRECT3DDEVICE9 devy);
	void Wrapping(D3DTEXTUREADDRESS tata, D3DTEXTUREFILTERTYPE filtertilter);
	void WrappingThing(int selection);
	void FilterThing(int filtrar);
	void BlendingThing(int selector);
	void RenderingComposite() override;
};
