#pragma once
#include <dinput.h>
/*#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include <d3dx9.h>
#pragma comment (lib, "d3dx9.lib")*/
#pragma comment (lib, "dinput8.lib")
#pragma comment (lib, "dxguid.lib")
#include"Jueguito.h"
// #include"MotorcitoApi.h"
class MOTORCITO_API Inputs
{
private:
	byte keys[256];
	byte otherKeys[256];
	LPDIRECTINPUT8 dip;
	LPDIRECTINPUTDEVICE8 KeyDev;
public:
	Inputs();
	~Inputs();
	void InputInitializer(/*HINSTANCE hInstance, HWND hWnd*/);
	bool Pressing(int thyKey);
	bool JustPressed(int thyKey);
	bool JustReleased(int thyKey);
	void update();
};
