#include "stdafx.h"
#include "MeshRenderer.h"


MeshRenderer::MeshRenderer()
{
	/*Jueguito* j = Jueguito::GetInstance();
	devP = j->GetDevice(); // CAMBIO PENDIENTE DE TESTING*/
}

MeshRenderer::MeshRenderer(SeinTexture* skinner) 
{
	texturizado = skinner;
}

MeshRenderer::~MeshRenderer()
{
}

void MeshRenderer::SetMesh(Meshy* meshu) 
{
	meshito = meshu;
}

void MeshRenderer::SetMaterial(Material* hidrogeno) 
{
	carbono = hidrogeno; // si esto lo ve un quimico, me asesina
}

Material* MeshRenderer::GetMaterial() 
{
	return carbono;
}

Meshy* MeshRenderer::GetMesh() 
{
	return meshito;
}

BoundingBox MeshRenderer::BringMesh() 
{
	return meshito->GetCopyBoundingBox();
}

void MeshRenderer::CreateEffect(LPD3DXEFFECT efectus)
{
	thyEffect = efectus;
}

LPD3DXEFFECT MeshRenderer::GetThyEffect() 
{
	return thyEffect;
}

void MeshRenderer::SetTexturization(LPDIRECT3DTEXTURE9 g_texture)
{
	if (!carbono) 
	{
		my_texture = g_texture;
	}
	else 
	{
		my_texture = carbono->GetTex();
	}
}

void MeshRenderer::SetTexturizationTwo(LPCWSTR seinTexture) 
{
	Jueguito *j = Jueguito::GetInstance();
	D3DXCreateTextureFromFile(j->GetDevice(),
		seinTexture,
		&my_texture);
}

void MeshRenderer::SetTexturizationThree(SeinTexture* thaTextura) 
{
	texturizado = thaTextura;
}

void MeshRenderer::SetDevPointer(LPDIRECT3DDEVICE9 devy) 
{
	devP = devy;
}

void MeshRenderer::Wrapping(D3DTEXTUREADDRESS tata, D3DTEXTUREFILTERTYPE filtertilter)
{
	ta = tata;
	filter = filtertilter;

}

void MeshRenderer::WrappingThing(int selection) 
{
	Jueguito* j = Jueguito::GetInstance();
	switch (selection)
	{
	default:
		break;
	case 1:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
		break;
	case 2:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
		break;
	case 3:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
		break;
	case 4:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_FORCE_DWORD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_FORCE_DWORD);
		break;
	case 5:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRROR);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRROR);
		break;
	case 6:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_MIRRORONCE);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_MIRRORONCE);
		break;
	}
}

void MeshRenderer::FilterThing(int filtrar) 
{
	Jueguito* j = Jueguito::GetInstance();
	switch (filtrar)
	{
	case 1:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_ANISOTROPIC);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_ANISOTROPIC);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_ANISOTROPIC);
		break;
	case 2:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_CONVOLUTIONMONO);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_CONVOLUTIONMONO);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_CONVOLUTIONMONO);
		break;
	case 3:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_FORCE_DWORD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_FORCE_DWORD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_FORCE_DWORD);
		break;
	case 4:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_GAUSSIANQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_GAUSSIANQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_GAUSSIANQUAD);
		break;
	case 5:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
		break;
	case 6:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
		break;
	case 7:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_PYRAMIDALQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_PYRAMIDALQUAD);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_PYRAMIDALQUAD);
		break;
	default:
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_NONE);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_NONE);
		j->GetDevice()->SetSamplerState(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
		break;
	}
}

void MeshRenderer::BlendingThing(int selector)
{
	Jueguito *j = Jueguito::GetInstance();
	thySelector = selector;
	switch (selector)
	{
	case 0:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		j->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		j->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		break;
	case 1:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		j->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		j->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
		break;
	case 2:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		j->GetDevice()->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		j->GetDevice()->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		break;
	default:
		j->GetDevice()->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
		break;
	}
}

void MeshRenderer::RenderingComposite()
{
	Jueguito* j = Jueguito::GetInstance();
	if (!carbono)
	{
		if (texturizado != NULL && my_texture == NULL)
		{
			j->GetDevice()->SetTexture(0, texturizado->GetSkin());
		}
		else if (texturizado == NULL && my_texture != NULL)
		{
			j->GetDevice()->SetTexture(0, my_texture);
		}
		else
		{
			j->GetDevice()->SetTexture(0, NULL);
		}
	}
	if (carbono)
	{
		carbono->SetBlend(/*j->GetDevice(),*/ carbono->getBlend());
		carbono->SetWrap(/*j->GetDevice(),*/ carbono->getWrap());
		carbono->SetFiltro(/*j->GetDevice(),*/ carbono->getFiltro());
		carbono->SetWrap(/*j->GetDevice(),*/ carbono->getWrap());
		j->GetDevice()->SetTexture(0, carbono->GetTex());
	}
	j->GetDevice()->SetTransform(D3DTS_WORLD, &thyMatrix);
	j->GetDevice()->SetFVF(MYFVF);
	j->GetDevice()->SetStreamSource(0, meshito->GetVb(), 0, sizeof(Vertice));
	j->GetDevice()->SetIndices(meshito->GetIb());
	j->GetDevice()->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, meshito->vertexes.size()/*4*/, 0, meshito->indexes.size()/*6*/ / 3);
}