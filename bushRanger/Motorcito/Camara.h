#pragma once
#include"Composite.h"
#include"Plane.h"
#include"string.h"
#include"Jueguito.h"
using namespace std;
class MOTORCITO_API Camara : public Composite
{
private:
	LPDIRECT3DDEVICE9 dev;
	D3DXMATRIX transMat;
	D3DXMATRIX rotXMat;
	D3DXMATRIX rotYMat;
	D3DXMATRIX rotZMat;
	D3DXMATRIX viewMatrix;
	D3DXMATRIX projectionMatrix;
	D3DXMATRIX viewProj;
	D3DXMATRIX mvp;
	Plane todos[6];
public:
	Camara(/*LPDIRECT3DDEVICE9 cosa*/); // CAMBIOS PENDIENTES DE TESTING
	Camara(D3DXVECTOR3 pos, D3DXVECTOR3 rot);
	~Camara();
	void SetPos(D3DXVECTOR3 posicion);
	void SetRot(D3DXVECTOR3 rotacion);
	D3DXVECTOR3 GetPos();
	D3DXVECTOR3 GetRot();
	D3DXMATRIX GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot);
	D3DXMATRIX GetProjectionMatrix();
	Plane* AllThePlanes();
	void UpdateComposite() override;
};

