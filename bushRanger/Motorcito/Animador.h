#pragma once
#include "MeshRenderer.h"
#include<chrono>
using namespace std::chrono;
class MOTORCITO_API Animador : public MeshRenderer
{
private:
	int tileXAmount;
	int tileYAmount;
	float aspectRatio;
	float width;
	float height;
	float tileWidth;
	float tileHeight;
	int tileRow;
	int tileCol;
	int lastFrameMs;
	float minFrameTime;
	float FramesPerSecond = 60;
	float realMs;
	float deltaTime;
	float spriteTime;
	vector<Meshy*> meshVector;
	vector<Vertice> misVertices;
	Meshy* seinMeshy[6];
	int animationNumber;
public:
	Animador(SeinTexture* skinner);
	~Animador();
	float GetMs();
	//void UpdateComposite() override;
	void SetTexturizationTwo(LPCWSTR seinTexture);
	void SetTexturizationThree(SeinTexture* thaTextura);
	void UpdateAnimation(float desacelerando);
	void AddAnimationFrame(/*LPDIRECT3DDEVICE9 dev,*/ int numColumns, int numRows);
};

