#pragma once
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include"MotorcitoApi.h"
#include"BoundingBox.h"
class MOTORCITO_API Component 
{
private:
	class Composite* parent;
protected:
	D3DXVECTOR3 _trasl;
	D3DXVECTOR3 _escal;
	D3DXVECTOR3 _rot;
	D3DXMATRIX _traslacion;
	D3DXMATRIX _escala;
	D3DXMATRIX _rotacion;
	D3DXMATRIX _transformationNeo;
	float _x;
	float _y;
	float scaleX;
	float scaleY;
	float scaleZ;
	float rotationX;
	float rotationY;
	float rotationZ;
	BoundingBox laCajita;
public:
	Component();
	virtual ~Component();
	virtual void Update();
	virtual void Rendering();
	// virtual void Move(D3DXVECTOR3 trasl, D3DXVECTOR3 escal, D3DXVECTOR3 rot);
	void Move(float x, float y);
	void SetModelPosition(float tX, float tY, float tZ);
	void SetModelScale(float sX, float sY, float sZ);
	void SetModelRotation(float rX, float rY, float rZ);
	void SetModelRotationX(float rX);
	void SetModelRotationY(float rY);
	D3DXVECTOR3 GetPosition() const;
	D3DXVECTOR3 GetScale() const;
	D3DXVECTOR3 GetRotation() const;
	void SetVectorPosition(D3DXVECTOR3 seinVector);
	float GetScaleX() const;
	float GetScaleY() const;
	float GetScaleZ() const;
	float GetRotationX() const;
	float GetRotationY() const;
	float GetRotationZ() const;
	virtual void createThyTransformationMatrix();
	D3DXMATRIX GetTransformationMatrix();
	// fjdshafkjdhaskjfldhasfkljhdasfklhjasfhdkjashfllas
	void SetParent(Composite* cosito);
	class Composite* GetParent();
	void SetBoundingBox(BoundingBox* objetus);
	BoundingBox* GetBoundingBox();
};