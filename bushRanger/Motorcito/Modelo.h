#pragma once
#include "../DX/Include/d3dx9.h"
#include <string>
#include "MotorcitoApi.h"

#include "Vertice.h"
#include <vector>
using namespace std;

//#include <d3dx9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3dx9.lib") //Incluyo la lib a mi proyecto
class MOTORCITO_API Modelo
{
private:
	LPDIRECT3DINDEXBUFFER9 ib;
	LPDIRECT3DVERTEXBUFFER9 vb;
public:
	Modelo();
	~Modelo();
	void LoadMesh(LPDIRECT3DDEVICE9 dev, const char* modelName);
	LPDIRECT3DVERTEXBUFFER9 GetVerticeBuffer();
	LPDIRECT3DINDEXBUFFER9 GetIndiceBuffer();
	vector<Vertice> vertexes;
	vector<WORD> indexes;
	vector<D3DXVECTOR3> positions;
	vector<D3DXVECTOR3> normals;
	vector<D3DXVECTOR2> uvs;
};

