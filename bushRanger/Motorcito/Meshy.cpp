#include "stdafx.h"
#include "Meshy.h"
#include "Vertice.h"



/*Meshy::Meshy()
{
}*/

Meshy::Meshy(/*LPDIRECT3DDEVICE9 dev,*/ const char* modelName)
{
	Jueguito* j = Jueguito::GetInstance();
	FILE* file;
	fopen_s(&file, modelName, "r");
	while (!feof(file))
	{
		char lineHeader[128];
		fscanf(file, "%s", lineHeader);
		if (strcmp(lineHeader, "v") == 0)
		{
			D3DXVECTOR3 position;
			fscanf(file, "%f %f %f\n", &position.x, &position.y, &position.z);
			laCajita.xMin = min(position.x, laCajita.xMin);
			laCajita.yMin = min(position.y, laCajita.yMin);
			laCajita.zMin = min(position.z, laCajita.zMin);
			laCajita.xMax = max(position.x, laCajita.xMax);
			laCajita.yMax = max(position.y, laCajita.yMax);
			laCajita.zMax = max(position.z, laCajita.zMax);
			positions.push_back(position);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			D3DXVECTOR3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			D3DXVECTOR2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			Vertice vertex;
			int posIndex, uvIndex, normalIndex;

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			indexes.push_back(vertexes.size());
			vertexes.push_back(vertex);

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			indexes.push_back(vertexes.size());
			vertexes.push_back(vertex);

			fscanf(file, "%d/%d/%d\n", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			indexes.push_back(vertexes.size());
			vertexes.push_back(vertex);
		}
	}

	laCajita.Refresh();

	j->GetDevice()->CreateVertexBuffer(vertexes.size() * sizeof(Vertice), 0, MYFVF, D3DPOOL_MANAGED, &vb, NULL);
	j->GetDevice()->CreateIndexBuffer(indexes.size() * sizeof(WORD), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib, NULL);

	VOID* lockedData = NULL;
	vb->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, vertexes.data(), vertexes.size() * sizeof(Vertice));
	vb->Unlock();

	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes.data(), indexes.size() * sizeof(WORD));
	ib->Unlock();

	j->GetDevice()->SetRenderState(D3DRS_ZENABLE, true);
	j->GetDevice()->SetRenderState(D3DRS_ZWRITEENABLE, true);
	j->GetDevice()->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);
}

Meshy::Meshy(/*LPDIRECT3DDEVICE9 dev*/)
{
	Jueguito* j = Jueguito::GetInstance();
	Vertice vertexes[] =
	{
		{ -0.6f,  0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f },    
		{  0.6f,  0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f },
		{  0.6f, -0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },
		{ -0.6f, -0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f }
	};
	WORD indexes[] = { 0, 1, 2, 2, 3, 0 };
	j->GetDevice()->CreateVertexBuffer(4 * sizeof(Vertice),
		D3DUSAGE_WRITEONLY,
		MYFVF,
		D3DPOOL_MANAGED,
		&vb,
		NULL);
	j->GetDevice()->CreateIndexBuffer(6 * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&ib,
		NULL);
	VOID *data;
	vb->Lock(0, 0, &data, 0);
	memcpy(data, vertexes, 4 * sizeof(Vertice));
	vb->Unlock();
	ib->Lock(0, 0, &data, 0);
	memcpy(data, indexes, 6 * sizeof(WORD));
	ib->Unlock();
}

Meshy::Meshy(Vertice seinVertice[], WORD seinIndexes[]/*, LPDIRECT3DDEVICE9 dev*/) 
{
	Jueguito* j = Jueguito::GetInstance();
	j->GetDevice()->CreateVertexBuffer(4 * sizeof(Vertice),
		D3DUSAGE_WRITEONLY,
		MYFVF,
		D3DPOOL_MANAGED,
		&vb,
		NULL);
	j->GetDevice()->CreateIndexBuffer(6 * sizeof(WORD),
		D3DUSAGE_WRITEONLY,
		D3DFMT_INDEX16,
		D3DPOOL_MANAGED,
		&ib,
		NULL);
	VOID *data;
	vb->Lock(0, 0, &data, 0);
	memcpy(data, seinVertice, 4 * sizeof(Vertice));
	vb->Unlock();
	ib->Lock(0, 0, &data, 0);
	memcpy(data, seinIndexes, 6 * sizeof(WORD));
	ib->Unlock();
	for (int i = 0; i < 4; i++)
	{
		vertexes.push_back(seinVertice[i]);
	}
	for (int i = 0; i < 6; i++) 
	{
		indexes.push_back(vertexes.size());
	}
}

Meshy::~Meshy()
{
	ib->Release();
	vb->Release();
}
void Meshy::LoadMesh(/*LPDIRECT3DDEVICE9 dev,*/ const char* modelName)
{
	Jueguito* j = Jueguito::GetInstance();
	FILE* file;
	fopen_s(&file, modelName, "r");
	while (!feof(file))
	{
		char lineHeader[128];
		fscanf(file, "%s", lineHeader);
		if (strcmp(lineHeader, "v") == 0)
		{
			D3DXVECTOR3 position;
			fscanf(file, "%f %f %f\n", &position.x, &position.y, &position.z);
			positions.push_back(position);
		}
		else if (strcmp(lineHeader, "vn") == 0)
		{
			D3DXVECTOR3 normal;
			fscanf(file, "%f %f %f\n", &normal.x, &normal.y, &normal.z);
			normals.push_back(normal);
		}
		else if (strcmp(lineHeader, "vt") == 0)
		{
			D3DXVECTOR2 uv;
			fscanf(file, "%f %f\n", &uv.x, &uv.y);
			uvs.push_back(uv);
		}
		else if (strcmp(lineHeader, "f") == 0)
		{
			Vertice vertex;
			int posIndex, uvIndex, normalIndex;

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			indexes.push_back(vertexes.size());
			vertexes.push_back(vertex);

			fscanf(file, "%d/%d/%d ", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			indexes.push_back(vertexes.size());
			vertexes.push_back(vertex);

			fscanf(file, "%d/%d/%d\n", &posIndex, &uvIndex, &normalIndex);
			vertex.x = positions[posIndex - 1].x;
			vertex.y = positions[posIndex - 1].y;
			vertex.z = positions[posIndex - 1].z;
			vertex.tu = uvs[uvIndex - 1].x;
			vertex.tv = uvs[uvIndex - 1].y;
			vertex.nx = normals[normalIndex - 1].x;
			vertex.ny = normals[normalIndex - 1].y;
			vertex.nz = normals[normalIndex - 1].z;
			indexes.push_back(vertexes.size());
			vertexes.push_back(vertex);
		}
	}

	j->GetDevice()->CreateVertexBuffer(vertexes.size() * sizeof(Vertice), 0, MYFVF, D3DPOOL_MANAGED, &vb, NULL);
	j->GetDevice()->CreateIndexBuffer(indexes.size() * sizeof(WORD), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &ib, NULL);

	VOID* lockedData = NULL;
	vb->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, vertexes.data(), vertexes.size() * sizeof(Vertice));
	vb->Unlock();

	ib->Lock(0, 0, (void**)&lockedData, 0);
	memcpy(lockedData, indexes.data(), indexes.size() * sizeof(WORD));
	ib->Unlock();

	j->GetDevice()->SetRenderState(D3DRS_ZENABLE, true);
	j->GetDevice()->SetRenderState(D3DRS_ZWRITEENABLE, true);
	j->GetDevice()->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESS);
}
BoundingBox* Meshy::GetBoundingBox() 
{
	return &laCajita;
}
BoundingBox Meshy::GetCopyBoundingBox() 
{
	return laCajita;
}
LPDIRECT3DVERTEXBUFFER9 Meshy::GetVb() 
{
	return vb;
}

LPDIRECT3DINDEXBUFFER9 Meshy::GetIb() 
{
	return ib;
}

/*bool Get3D()
{
	return soy3D;
}*/