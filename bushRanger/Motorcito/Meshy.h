#pragma once
#include"MotorcitoApi.h"
#include"Vertice.h"
#include<vector>
#include "../DX/Include/d3dx9.h"
#include <string>
#include "MotorcitoApi.h"
#include "BoundingBox.h"
#include "Jueguito.h"
using namespace std;
class MOTORCITO_API Meshy
{
private:
	LPDIRECT3DVERTEXBUFFER9 vb;
	LPDIRECT3DINDEXBUFFER9 ib;
	BoundingBox laCajita;
public:
	Meshy();
	Meshy(/*LPDIRECT3DDEVICE9 dev,*/ const char* modelName);
	Meshy(Vertice seinVertice[], WORD seinIndexes[]/*, LPDIRECT3DDEVICE9 dev*/);
	~Meshy();
	void LoadMesh(/*LPDIRECT3DDEVICE9 dev,*/ const char* modelName);
	BoundingBox* GetBoundingBox();
	BoundingBox GetCopyBoundingBox();
	LPDIRECT3DVERTEXBUFFER9 GetVb();
	LPDIRECT3DINDEXBUFFER9 GetIb();
	vector<Vertice> vertexes;
	vector<WORD> indexes;
	vector<D3DXVECTOR3> positions;
	vector<D3DXVECTOR3> normals;
	vector<D3DXVECTOR2> uvs;
};

