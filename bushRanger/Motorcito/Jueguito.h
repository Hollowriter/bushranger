#pragma once
#include <d3d9.h> //Busca el header de directx en los path
#pragma comment (lib, "d3d9.lib") //Incluyo la lib a mi proyecto
#include <d3dx9.h>
#pragma comment (lib, "d3dx9.lib")
#include"MotorcitoApi.h"
#include"Conector.h"
/*#include"Molde.h"
#include"JugadorPrueba.h"
#include"EnemigoPrueba.h"
#include"GetViewMatrix.h"
#include"Camara.h"
#include"Composite.h"
#include"Component.h"
#include"Animador.h"
#include"MeshRenderer.h"
#include"TileRenderer.h"
#include"BoundingBox.h"*/

class MOTORCITO_API Jueguito
{
private:
	Jueguito();
	static Jueguito* seinInstance;
	HWND hWnd;
	HINSTANCE _hInstance;
public:
	LPDIRECT3DDEVICE9 dev;
	/*
	Inputs* teclitas;
	Meshy* mesh;
	Meshy* mesh2;
	Tiler* tiles;
	Animador* thyAnimator;
	MeshRenderer* seinMesh;
	MeshRenderer* seinMesh2;
	TileRenderer* seinTiler;
	IDirect3DTexture9* g_texture = NULL;
	IDirect3DTexture9* g_texture2 = NULL;
	D3DTEXTUREADDRESS ta;
	D3DTEXTUREFILTERTYPE filter;
	Camara* camarita;
	*/
	~Jueguito();
	HWND GethWnd(); // ver
	HINSTANCE GethInstance(); // ver
	static Jueguito* GetInstance(); // ver 
	LPDIRECT3DDEVICE9 GetDevice(); // ver
	virtual void FrameEvent(); // ver
	void Run(_In_     HINSTANCE hInstance, _In_     int       nCmdShow, Conector* conection); // ver
};

