#include "stdafx.h"
#include "Molde.h"
#include"Vertice.h" // aca esta el problema pero no se que es

Molde::Molde()
{
	trasl = D3DXVECTOR3(0, 0, 0);
	rot = D3DXVECTOR3(0, 0, 0);
	escal = D3DXVECTOR3(1, 1, 1);
	clasees = 0;
	horrible = true;
}

Molde::Molde(D3DXVECTOR3 tr, D3DXVECTOR3 ro, D3DXVECTOR3 es, int classss) 
{
	trasl = tr;
	rot = ro;
	escal = es;
	clasees = classss;
	horrible = true;
}

Molde::~Molde()
{
}

void Molde::SetMesh(Meshy* meshu)
{
	thyMesh = meshu;
}

void Molde::SetInputs(Inputs* thoseInputs)
{
	teclas = thoseInputs;
}

void Molde::SetTexturization(IDirect3DTexture9* g_texture)
{
	my_texture = g_texture;
}

void Molde::Wrapping(D3DTEXTUREADDRESS tata, D3DTEXTUREFILTERTYPE filtertilter) 
{
	ta = tata;
	filter = filtertilter;
}

void Molde::BlendingThing(int selector) 
{
	thySelector = selector;
}

void Molde::Movimiento() 
{
}

D3DXMATRIX Molde::GetModelMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot, D3DXVECTOR3 sca)
{
	D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = pos.x;
	transMat._42 = pos.y;
	transMat._43 = pos.z;

	D3DXMATRIX scaMat;
	D3DXMatrixIdentity(&scaMat);
	scaMat._11 = sca.x;
	scaMat._22 = sca.y;
	scaMat._33 = sca.z;

	D3DXMATRIX rotXMat;
	rotXMat._11 = 1;
	rotXMat._22 = cos(rot.x);
	rotXMat._23 = -sin(rot.x);
	rotXMat._32 = sin(rot.x);
	rotXMat._33 = cos(rot.x);

	D3DXMATRIX rotYMat;
	rotYMat._11 = cos(rot.y);
	rotYMat._13 = sin(rot.y);
	rotYMat._31 = -sin(rot.y);
	rotYMat._33 = cos(rot.y);

	D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(rot.z);
	rotZMat._12 = sin(rot.z);
	rotZMat._21 = -sin(rot.z);
	rotZMat._22 = cos(rot.z);

	return scaMat * rotZMat * transMat;
}

void Molde::SetTrasl(D3DXVECTOR3 posicion) 
{
	trasl = posicion;
}

void Molde::SetEscal(D3DXVECTOR3 escalado) 
{
	escal = escalado;
}

void Molde::SetRot(D3DXVECTOR3 rotacion) 
{
	rot = rotacion;
}

D3DXVECTOR3 Molde::GetTrasl() 
{
	return trasl;
}

D3DXVECTOR3 Molde::GetEscal() 
{
	return escal;
}

D3DXVECTOR3 Molde::GetRot() 
{
	return rot;
}

void Molde::Draw(LPDIRECT3DDEVICE9 dev)
{
	D3DXMATRIX thyMatrix;
	D3DXMatrixIdentity(&thyMatrix);
	thyMatrix = GetModelMatrix(trasl, rot, escal); 
	dev->SetTransform(D3DTS_WORLD, &thyMatrix);
	dev->SetFVF(MYFVF);
	dev->SetStreamSource(0, thyMesh->GetVb(), 0, sizeof(Vertice));
	dev->SetIndices(thyMesh->GetIb());
	// Setting the texture
	dev->SetTexture(0, my_texture);
	// Wrapping
	dev->SetSamplerState(0, D3DSAMP_ADDRESSU, ta);
	dev->SetSamplerState(0, D3DSAMP_ADDRESSV, ta);
	dev->SetSamplerState(0, D3DSAMP_MAGFILTER, filter);
	dev->SetSamplerState(0, D3DSAMP_MINFILTER, filter);
	dev->SetSamplerState(0, D3DSAMP_MIPFILTER, filter);
	// Blending
	switch (thySelector)
	{
	case 0:
		dev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
		dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ONE);
		break;
	case 1:
		dev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ZERO);
		dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR);
		break;
	case 2:
		dev->SetRenderState(D3DRS_ALPHABLENDENABLE, true);
		dev->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
		dev->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
		break;
	default:
		dev->SetRenderState(D3DRS_ALPHABLENDENABLE, false);
	}
	dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
}
