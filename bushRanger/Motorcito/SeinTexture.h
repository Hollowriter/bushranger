#pragma once
#ifndef SEINTEXTURE_H
#define SEINTEXTURE_H

#include <d3d9.h>
#pragma comment (lib, "d3d9.lib")
#include<String>
#include"Jueguito.h"
#include"MotorcitoApi.h"
using namespace std;
class MOTORCITO_API SeinTexture 
{
private:
	IDirect3DTexture9* thySkin;
public:
	SeinTexture();
	SeinTexture(LPWSTR skinny);
	~SeinTexture();
	IDirect3DTexture9* GetSkin();
};

#endif
