#include "stdafx.h"
#include "Jueguito.h"
/*#include "BoundingBox.h"
#include "Conector.h"*/


Jueguito* Jueguito::seinInstance = nullptr;

Jueguito::Jueguito()
{
	Jueguito::seinInstance = this;
}

HWND Jueguito::GethWnd()
{
	return hWnd;
}

HINSTANCE Jueguito::GethInstance()
{
	return _hInstance;
}
Jueguito * Jueguito::GetInstance()
{
	if (seinInstance == 0)
	{
		seinInstance = new Jueguito();
	}
	return seinInstance;
}
LPDIRECT3DDEVICE9 Jueguito::GetDevice()
{
	return dev;
}
void Jueguito::FrameEvent() 
{
}
Jueguito::~Jueguito()
{
}
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam);
void Jueguito::Run(_In_     HINSTANCE hInstance, _In_     int       nCmdShow, Conector* conection)
{
	//Creamos la clase de la ventana
	WNDCLASSEX wcex;
	_hInstance = hInstance;
	//Iniciamos sus valores en 0
	ZeroMemory(&wcex, sizeof(WNDCLASSEX));


	wcex.cbSize = sizeof(WNDCLASSEX); //Tama�o en bytes
	wcex.style = CS_HREDRAW | CS_VREDRAW; //Estilo de la ventana
	wcex.lpfnWndProc = WndProc; //Funcion de manejo de mensajes de ventana
	wcex.hInstance = hInstance; //Numero de instancia
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW); //Cursor del mouse
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1); //Color de fondo
	wcex.lpszClassName = L"GameWindowClass"; //Nombre del tipo (clase) de ventana

											 //Registro mi tipo de ventana en windows
	RegisterClassEx(&wcex);

	//Creo la ventana, recibiendo el numero de ventana
	HWND hWnd = CreateWindowEx(0, //Flags extra de estilo
		L"GameWindowClass", //Nombre del tipo de ventana a crear
		L"CosaHorrible", //Titulo de la barra
		WS_OVERLAPPEDWINDOW, //Flags de estilos
		0, //X
		0, //Y
		640, //Ancho
		480, //Alto
		NULL, //Ventana padre
		NULL, //Menu
		hInstance, //Numero de proceso
		NULL); //Flags de multi ventana

	ShowWindow(hWnd, nCmdShow); //Muestro la ventana
	UpdateWindow(hWnd); //La actualizo para que se vea

						//Me comunico con directx por una interfaz, aca la creo
	LPDIRECT3D9 d3d = Direct3DCreate9(D3D_SDK_VERSION);

	//Creo los parametros de los buffers de dibujado (pantalla)
	D3DPRESENT_PARAMETERS d3dpp;
	ZeroMemory(&d3dpp, sizeof(D3DPRESENT_PARAMETERS));
	d3dpp.Windowed = true;
	d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
	d3dpp.hDeviceWindow = hWnd;

	//Creo la interfaz con la placa de video
	d3d->CreateDevice(D3DADAPTER_DEFAULT, //Cual placa de video
		D3DDEVTYPE_HAL, //Soft o hard
		hWnd, //Ventana
		D3DCREATE_HARDWARE_VERTEXPROCESSING, //Proceso de vertices por soft o hard
		&d3dpp, //Los parametros de buffers
		&dev); //El device que se crea LA PUTA QUE TE RECONTRA PARIO
	dev->SetRenderState(D3DRS_LIGHTING, FALSE);
	Conector* conector = conection;
	conector->LoadContent();
	/*D3DXCreateTextureFromFile(dev,
		L"TracerGun.png",
		&g_texture);
	D3DXCreateTextureFromFile(dev,
		L"BoxTest.png",
		&g_texture2);*/
	/*Vertice vertexes[] =
	{
		{ -0.6f,  0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f },
		{ 0.6f,  0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f },
		{ 0.6f, -0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f },
		{ -0.6f, -0.6f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f }
	};
	WORD indexes[] = { 0, 1, 2, 2, 3, 0 };
	LPD3DXEFFECT seinEffect;
	D3DXCreateEffectFromFile(
		dev, L"EffectDamage.fx", NULL, NULL,
		D3DXSHADER_ENABLE_BACKWARDS_COMPATIBILITY,
		NULL, &seinEffect, NULL);
	// esto es para el ejercicio de vertex
	teclitas = new Inputs();
	camarita = new Camara(dev);
	mesh = new Meshy(dev, "TestCube.obj");
	mesh2 = new Meshy(vertexes, indexes, dev);
	Material* cosa = new Material(L"BoxTest.png", dev);
	Material* cosa2 = new Material(L"walk.png", dev);
	cosa->SelectBlend(3);
	cosa->SelectFiltro(1);
	cosa->SelectWrap(1);
	cosa2->SelectBlend(5);
	cosa2->SelectFiltro(1);
	cosa2->SelectWrap(1);
	teclitas->InputInitializer(hInstance, hWnd);*/

	/*tiles = new Tiler();
	seinTiler = new TileRenderer(dev);
	seinTiler->SetTiler(tiles);
	seinTiler->GetTiler()->AddPalette(g_texture2);
	seinTiler->GetTiler()->AddPalette(g_texture);
	seinTiler->GetTiler()->SetSize(5, 5);*/

	/*Animador* animatronico = new Animador();
	animatronico->SetDevPointer(dev);
	//animatronico->SetMaterial(cosa2);
	animatronico->SetMesh(mesh2);
	animatronico->SetTexturizationTwo(L"walk.png");
	// animatronico->SetTexturization(g_texture2);
	animatronico->AddAnimationFrame(dev, 3, 3);*/

	/*seinMesh = new MeshRenderer();
	seinMesh->SetDevPointer(dev);
	seinMesh->SetMaterial(cosa);
	seinMesh->SetMesh(mesh);
	seinMesh->SetBoundingBox(mesh->GetBoundingBox());
	//seinMesh->SetTexturization(g_texture2);
	seinMesh->CreateEffect(seinEffect);*/

	/*seinMesh2 = new MeshRenderer();
	seinMesh2->SetDevPointer(dev);
	seinMesh2->SetMaterial(cosa);
	seinMesh2->SetMesh(mesh);
	seinMesh2->SetBoundingBox(mesh->GetBoundingBox());

	seinMesh->Add(seinMesh2);*/
	//BoundingBox hola = seinMesh->GetBoundingBox();

	//seinMesh->Wrapping(ta, filter);
	//seinMesh->BlendingThing(0);
	/*camarita->Move(D3DXVECTOR3(0, 0, -10), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(0, 0, 0));
	animatronico->Move(D3DXVECTOR3(3, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(1, 1, 1));
	seinMesh->Move(D3DXVECTOR3(5, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(1, 1, 1));
	seinMesh2->Move(D3DXVECTOR3(5, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(1, 1, 1));
	BoundingBox* hola = seinMesh->GetBoundingBox();*/
	/*seinMesh2->Move(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(0, 0, 0));*/
	//seinTiler->Move(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(0, 0, 0));
	/*seinMesh->GetComponent<MeshRenderer>()->Move(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(0, 0, 0));*/
	// seinMesh2->Move(D3DXVECTOR3(0, 0, 0), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(0, 0, 0));

	while (true)
	{
		MSG msg;

		//Saco un mensaje de la cola de mensajes si es que hay
		//sino continuo
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
		{
			break;
		}
		/*coso = coso + 0.1f;
		seinMesh->Move(D3DXVECTOR3(coso, 2, 2), D3DXVECTOR3(5, 5, 5), D3DXVECTOR3(1, 1, 1));
		hola = seinMesh->GetBoundingBox();*/
		//Actualizar
		dev->Clear(0, NULL, D3DCLEAR_TARGET, D3DCOLOR_ARGB(0, 100, 0, 100), 1.0f, 0);
		dev->BeginScene();
		/*D3DXHANDLE handleo = seinMesh->GetThyEffect()->GetTechniqueByName("RedColor");
		seinMesh->GetThyEffect()->SetTechnique(handleo);
		UINT passses = 1;
		seinMesh->GetThyEffect()->SetFloat("num", 0.1f);
		seinMesh->GetThyEffect()->SetVector("_Color", &D3DXVECTOR4(0, 2.2f, 8.5f, 0));
		D3DXMATRIX thyMvp = seinMesh->GetThyMatrix() * camarita->GetViewMatrix(camarita->GetPos(), camarita->GetRot()) * camarita->GetProjectionMatrix();
		seinMesh->GetThyEffect()->SetMatrix("mvp", &thyMvp);
		seinMesh->GetThyEffect()->Begin(&passses, 0);
		for (UINT pass = 0; pass < passses; pass++) 
		{
			seinMesh->GetThyEffect()->BeginPass(pass);
			dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2); // test pending
			seinMesh->GetThyEffect()->EndPass();
		}*/

		/*seinEffect->SetVector("_Color", &D3DXVECTOR4(1, 1.5f, 8.5f, 0));
		seinEffect->SetMatrix("mvp", &seinMesh->GetThyMatrix()); // despues ver la matriz mvp
		seinEffect->Begin(&passses, 0);
		seinEffect->BeginPass(0);
		seinEffect->EndPass();
		seinEffect->End();*/

		/*camarita->Update();
		teclitas->update();
		seinMesh->Update();
		animatronico->Update();
		//seinTiler->Update();
		seinMesh->Rendering();
		animatronico->Rendering();*/
		//seinTiler->Rendering();
		//seinMesh->GetThyEffect()->End();
		conector->ThyFrames();
		dev->EndScene();
		dev->Present(NULL, NULL, NULL, NULL);
	}
	dev->Release();
	d3d->Release();
	//delete seinTiler;
	//delete tiles;
	/*delete animatronico;
	delete seinMesh2;
	delete seinMesh;
	delete mesh2;
	delete mesh;
	delete cosa2;
	delete cosa;
	delete teclitas;
	delete camarita;*/
}
//Manejo de mensajes por ventana
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	if (message == WM_DESTROY)
	{
		//Si destruyeron esta ventana (cerraron) le pido
		//a windows que cierre la app
		PostQuitMessage(0);
		return 0;
	}

	//Si no maneje el mensaje antes, hago el comportamiento por defecto
	return DefWindowProc(hWnd, message, wParam, lParam);
}