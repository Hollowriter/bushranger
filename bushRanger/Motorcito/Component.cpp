#include"stdafx.h"
#include"Component.h"
#include"Composite.h"
Component::Component() : _trasl(0, 0, 0), scaleX(1), scaleY(1), scaleZ(1), _escal(1, 1, 1)
{
	D3DXMatrixIdentity(&_escala);
	D3DXMatrixIdentity(&_traslacion);
	D3DXMatrixIdentity(&_rotacion);
}
Component::~Component() 
{
}
void Component::Update() 
{
}
void Component::Rendering() 
{
	createThyTransformationMatrix();
}
/*void Component::Move(D3DXVECTOR3 trasl, D3DXVECTOR3 escal, D3DXVECTOR3 rot) 
{
	_trasl += trasl;
	_escal += escal;
	_rot += rot;*/
	//TODO: Hacer que GetBoundingBox retorne un puntero (YO: Check)
	//TODO: Recalcular el bounding box partido del original del modelo y los hijos (YO: check, creo)
	//laCajita = GetBoundingBox()->Transform(_trasl, _rot, _escal);
	//TODO: Hacer recursividad para recalcular los BB de todos los ancestros
	//TODO: Hacer que al recalcular el bounding box tenga en cuenta los originales, ya que puede achicarse el BB
	/*if (GetParent())
	{
		BoundingBox *papaBB = GetParent()->GetComponent<BoundingBox>();
		papaBB->Combine(laCajita);
	}
	laCajita.Refresh();*/
// }
void Component::Move(float x, float y) 
{
	_x = x;
	_y = y;
}
void Component::SetModelPosition(float tX, float tY, float tZ)
{
	_trasl.x = tX;
	_trasl.y = tY;
	_trasl.z = tZ;
	D3DXMatrixTranslation(&_traslacion, tX, tY, tZ);
}
void Component::SetModelScale(float sX, float sY, float sZ)
{
	scaleX = sX;
	scaleY = sY;
	scaleZ = sZ;
	_escal.x = sX;
	_escal.y = sY;
	_escal.z = sZ;
	D3DXMatrixScaling(&_escala, scaleX, scaleY, scaleZ);
}
void Component::SetModelRotation(float rX, float rY, float rZ)
{
	rotationX = rX;
	rotationY = rY;
	rotationZ = rZ;
	_rot.x = rX;
	_rot.y = rY;
	_rot.z = rZ;
	D3DXMatrixRotationZ(&_rotacion, rotationZ);
}
void Component::SetModelRotationX(float rX)
{
	rotationX = rX;
	D3DXMatrixRotationX(&_rotacion, rotationX);
}
void Component::SetModelRotationY(float rY)
{
	rotationY = rY;
	D3DXMatrixRotationY(&_rotacion, rotationY);
}
D3DXVECTOR3 Component::GetPosition() const
{
	return _trasl;
}
D3DXVECTOR3 Component::GetScale() const
{
	return _escal;
}
D3DXVECTOR3 Component::GetRotation() const
{
	return _rot;
}
void Component::SetVectorPosition(D3DXVECTOR3 seinVector)
{
	SetModelPosition(seinVector.x, seinVector.y, seinVector.z);
}
float Component::GetScaleX() const
{
	return _escal.x;
}
float Component::GetScaleY() const
{
	return scaleY;
}
float Component::GetScaleZ() const
{
	return scaleZ;
}
float Component::GetRotationX() const
{
	return rotationX;
}
float Component::GetRotationY() const
{
	return rotationY;
}
float Component::GetRotationZ() const
{
	return rotationZ;
}
void Component::createThyTransformationMatrix()
{
	D3DXMatrixIdentity(&_transformationNeo);
	D3DXMatrixMultiply(&_transformationNeo, &_traslacion, &_transformationNeo);
	D3DXMatrixMultiply(&_transformationNeo, &_rotacion, &_transformationNeo);
	D3DXMatrixMultiply(&_transformationNeo, &_escala, &_transformationNeo);

	if (parent != NULL)
	{
		_transformationNeo = (parent->_transformationNeo * _transformationNeo);
	}
}
D3DXMATRIX Component::GetTransformationMatrix()
{
	createThyTransformationMatrix();
	return _transformationNeo;
}
void Component::SetParent(Composite* patent) 
{
	this->parent = patent;
}
void Component::SetBoundingBox(BoundingBox* objetus)
{
	laCajita = *objetus;
}
BoundingBox* Component::GetBoundingBox() 
{
	return &laCajita;
}
Composite* Component::GetParent() 
{
	return parent;
}