#pragma once
#include<iostream>
#include<vector>
#include"Composite.h"
using namespace std;
class Tiler
{
protected:
	vector<vector<int>> a;
	vector<LPDIRECT3DTEXTURE9> seinTextureMap;
	int filaTileMap;
	int columnaTileMap;
public:
	Tiler();
	~Tiler();
	void SetSize(int m, int l); // Filas y columnas TileMap
	void AddPalette(LPDIRECT3DTEXTURE9 aTexture);
	void SetTile(int m, int l, int value);
	vector<LPDIRECT3DTEXTURE9> GetPaletteVector();
	vector<vector<int>> GetA();
	int GetTile(int m, int l); // Filas y columnas TileMap
	int GetFilas();
	int GetColumnas();
};

