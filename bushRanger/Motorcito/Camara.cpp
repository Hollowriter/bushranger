#include "stdafx.h"
#include "Camara.h"


Camara::Camara(/*LPDIRECT3DDEVICE9 cosa*/)
{
	Jueguito *j = Jueguito::GetInstance();
	_trasl = D3DXVECTOR3(0, 0, 0);
	_rot = D3DXVECTOR3(0, 0, 0);
	dev = j->GetDevice(); // CAMBIOS PENDIENTES DE TESTING
}


Camara::~Camara()
{
}

Camara::Camara(D3DXVECTOR3 pos, D3DXVECTOR3 rot) 
{
	_trasl = pos;
	_rot = rot;
}

void Camara::SetPos(D3DXVECTOR3 posicion) 
{
	_trasl = posicion;
}

void Camara::SetRot(D3DXVECTOR3 rotacion) 
{
	_rot = rotacion;
}

D3DXVECTOR3 Camara::GetPos() 
{
	return _trasl;
}

D3DXVECTOR3 Camara::GetRot() 
{
	return _rot;
}

D3DXMATRIX Camara::GetViewMatrix(D3DXVECTOR3 pos, D3DXVECTOR3 rot)
{
	// D3DXMATRIX transMat;
	D3DXMatrixIdentity(&transMat);
	transMat._41 = -pos.x;
	transMat._42 = -pos.y;
	transMat._43 = -pos.z;

	// D3DXMATRIX rotXMat;
	D3DXMatrixIdentity(&rotXMat);
	rotXMat._11 = 1;
	rotXMat._22 = cos(-rot.x);
	rotXMat._23 = -sin(-rot.x);
	rotXMat._32 = sin(-rot.x);
	rotXMat._33 = cos(-rot.x);

	// D3DXMATRIX rotYMat;
	D3DXMatrixIdentity(&rotYMat);
	rotYMat._11 = cos(-rot.y);
	rotYMat._13 = sin(-rot.y);
	rotYMat._31 = -sin(-rot.y);
	rotYMat._33 = cos(-rot.y);

	// D3DXMATRIX rotZMat;
	D3DXMatrixIdentity(&rotZMat);
	rotZMat._11 = cos(-rot.z);
	rotZMat._12 = sin(-rot.z);
	rotZMat._21 = -sin(-rot.z);
	rotZMat._22 = cos(-rot.z);

	viewMatrix = transMat * rotZMat;

	return transMat * rotZMat;
}
D3DXMATRIX Camara::GetProjectionMatrix() 
{
	return projectionMatrix;
}
Plane* Camara::AllThePlanes() 
{
	// viewProj = viewMatrix * projectionMatrix;
	D3DXMatrixMultiply(&projectionMatrix, &viewMatrix, &viewProj);
	todos[0].SetPlane(
		viewProj._14 - viewProj._11,
		viewProj._24 - viewProj._21,
		viewProj._34 - viewProj._31,
		viewProj._44 - viewProj._41
		);
	todos[1].SetPlane(
		viewProj._14 + viewProj._11,
		viewProj._24 + viewProj._21,
		viewProj._34 + viewProj._31,
		viewProj._44 + viewProj._41
	);
	todos[2].SetPlane(
		viewProj._14 - viewProj._12,
		viewProj._24 - viewProj._22,
		viewProj._34 - viewProj._32,
		viewProj._44 - viewProj._42
	);
	todos[3].SetPlane(
		viewProj._14 + viewProj._12,
		viewProj._24 + viewProj._22,
		viewProj._34 + viewProj._32,
		viewProj._44 + viewProj._42
	);
	todos[4].SetPlane(
		viewProj._14 - viewProj._13,
		viewProj._24 - viewProj._23,
		viewProj._34 - viewProj._33,
		viewProj._44 - viewProj._43
	);
	todos[5].SetPlane(
		viewProj._13,
		viewProj._23,
		viewProj._33,
		viewProj._43
	);

	/*for (int i = 0; i < 6; i++) {
		D3DXPlaneNormalize(&todos[i], &todos[i]); // revisar
	}*/ // pendiente
	return todos;
}
void Camara::UpdateComposite() 
{
	viewMatrix = GetViewMatrix(
		_trasl,
		_rot);
	D3DXMatrixPerspectiveFovLH(
		&projectionMatrix,
		D3DXToRadian(60),
		(float)640 / 480, //ancho de la pantalla dividido por el alto
		0.0f, //Distancia minima de vision
		50); //Distancia maxima de vision /*Estos datos estan hardcodeados*/ /*Mover esto a un constructor asi se llama una sola vez*/
	viewMatrix = transMat * rotZMat;
	dev->SetTransform(D3DTS_VIEW, &viewMatrix);
	dev->SetTransform(D3DTS_PROJECTION, &projectionMatrix);
}
