#include "stdafx.h"
#include "Animador.h"


Animador::Animador(SeinTexture* skinner):MeshRenderer(skinner)
{
	animationNumber = 0;
	lastFrameMs = GetMs();
}


Animador::~Animador()
{
}

float Animador::GetMs() 
{
	time_point<system_clock> actualTimer = system_clock::now();
	system_clock::duration durando = actualTimer.time_since_epoch();
	milliseconds ms = duration_cast<milliseconds>(durando);
	return ms.count();
}

/*void Animador::UpdateComposite() 
{
	float actualMs = GetMs();
	float deltaTime = (actualMs - lastFrameMs) / 1000.0f;
	lastFrameMs = actualMs;
}*/

void Animador::SetTexturizationTwo(LPCWSTR seinTexture)
{
	MeshRenderer::SetTexturizationTwo(seinTexture);
	animationNumber = 0;
	lastFrameMs = GetMs();
}

void Animador::SetTexturizationThree(SeinTexture* thaTextura) 
{
	MeshRenderer::SetTexturizationThree(thaTextura);
	animationNumber = 0;
	lastFrameMs = GetMs();
}

void Animador::UpdateAnimation(float desacelerando) 
{
	realMs = GetMs();
	deltaTime = (realMs - lastFrameMs) / 1000.0f;
	lastFrameMs = realMs;
	spriteTime += deltaTime;
	if (spriteTime >= desacelerando)
	{
		spriteTime = 0;
		if (animationNumber < meshVector.size() - 1)
			animationNumber++;
		else
			animationNumber = 0;
		SetMesh(meshVector[animationNumber]);
	}
}

void Animador::AddAnimationFrame(/*LPDIRECT3DDEVICE9 dev,*/ int numColumns, int numRows)
{
	tileXAmount = numColumns;
	tileYAmount = numRows;
	aspectRatio = (float)tileXAmount / (float)tileYAmount;
	width = 1 / aspectRatio;
	tileWidth = 1.0f / tileXAmount;
	tileHeight = 1.0f / tileYAmount;
	tileRow = 1;
	tileCol = 1;
	WORD indexes[] = { 3,0,1,3,1,2 };
	for (int i = 0; i < tileXAmount; i++) 
	{
		tileCol++;
		Vertice vertexes[4] =
		{
			{ 0.00f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, tileWidth*tileCol     , tileHeight * (tileRow + 1) },
			{ -width, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, tileWidth*(tileCol - 1) , tileHeight * (tileRow + 1) },
			{ -width, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, tileWidth*(tileCol - 1) , tileHeight * tileRow },
			{ 0.00f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f, tileWidth*tileCol     , tileHeight * tileRow }
		};
		meshVector.push_back(new Meshy(vertexes, indexes));
	}
	tileRow++;
}