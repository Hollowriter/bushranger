#include "stdafx.h"
#include "TileRenderer.h"

TileRenderer::TileRenderer(LPDIRECT3DDEVICE9 deving)
{
	dev = deving;
}
TileRenderer::~TileRenderer()
{
}
void TileRenderer::SetTiler(Tiler* thyTiles) 
{
	seinTiles = thyTiles;
}
Tiler* TileRenderer::GetTiler() 
{
	return seinTiles;
}
void TileRenderer::RenderingComposite() 
{
	float anchoTile = 1.0f;
	float altoTile = 1.0f;

	D3DXMATRIX matSca;
	D3DXMatrixScaling(&matSca, anchoTile, altoTile, 1);

	for (int i = 0; i < seinTiles->GetFilas(); i++) 
	{
		vector<int> fila = seinTiles->GetA()[i];
		for (int h = 0; h < seinTiles->GetColumnas(); h++) 
		{
			int thyCelda = fila.at(h);
			LPDIRECT3DTEXTURE9 tex = seinTiles->GetPaletteVector()[thyCelda]; // Vector subscript out of range (HABLAR CON EL PROFE)
			dev->SetTexture(0, tex);

			float posX = anchoTile * h;
			float posY = altoTile * i;
			D3DXMATRIX matTrans;
			D3DXMatrixTranslation(&matTrans, posX, -posY, 0);

			D3DXMATRIX mat = matSca * matTrans;
			dev->SetTransform(D3DTS_WORLD, &mat);

			dev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, 0, 4, 0, 2);
		}
	}
}