#include "stdafx.h"
#include "Enemy.h"


Enemy::Enemy()
{
	_x = 8;
	_y = 8;
	_health = 10;
	_shooter = false;
	_hv = false;
}
Enemy::Enemy(int x, int y, bool shooter, bool hv, int health) 
{
	_x = x;
	_y = y;
	_shooter = shooter;
	_hv = hv;
	_health = health;
}
void Enemy::SetX(int x)
{
	_x = x;
}
void Enemy::SetY(int y)
{
	_y = y;
}
void Enemy::SetHealthEnemy(int health)
{
	_health = health;
}
void Enemy::SetHv(bool hv)
{
	_hv = hv;
}
void Enemy::SetShooter(bool shooting) 
{
	_shooter = shooting;
}
void Enemy::MoveAndShoot() 
{
}
int Enemy::GetX()
{
	return _x;
}
int Enemy::GetY()
{
	return _y;
}
bool Enemy::GetShooter() 
{
	return _shooter;
}
int Enemy::GetHealthEnemy() 
{
	return _health;
}
bool Enemy::GetHv() 
{
	return _hv;
}

Enemy::~Enemy()
{
}
