#include "stdafx.h"
#include "Wall.h"


Wall::Wall()
{
	_x = 0;
	_y = 0;
}
Wall::Wall(int x, int y) 
{
	_x = x;
	_y = y;
}
void Wall::SetX(int x)
{
	_x = x;
}
void Wall::SetY(int y)
{
	_y = y;
}
void Wall::Collision(Player jugador, Bullet bala) 
{
	if (_x == bala.GetX() && _y == bala.GetY())
	{
		bala.SetX(0);
		bala.SetY(0);
		bala.SetShoot(0);
		bala.SetTimer(50);
	}
	if (_x == jugador.GetX() && _y == jugador.GetY()) 
	{
	}
}
int Wall::GetX()
{
	return _x;
}
int Wall::GetY()
{
	return _y;
}

Wall::~Wall()
{
}
