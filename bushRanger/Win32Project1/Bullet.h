#pragma once
#ifndef BULLET_H
#define BULLET_H
#include<iostream>
#include"../Motorcito/Composite.h"
class Bullet : public Composite
{
private:
	int _x;
	int _y;
	bool _shoot;
	int _timer;
public:
	Bullet();
	~Bullet();
	void Move();
	void SetX(int x);
	void SetY(int y);
	void SetShoot(bool shoot);
	void SetTimer(int timer);
	int GetX();
	int GetY();
	int GetShoot();
	int GetTimer();
};

#endif
