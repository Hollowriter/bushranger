#pragma once
#ifndef WALL_H
#define WALL_H
#include<iostream>
#include"../Motorcito/Composite.h"
#include"Player.h"
#include"Bullet.h"
class Wall : public Composite
{
private:
	int _x;
	int _y;
public:
	Wall();
	Wall(int x, int y);
	~Wall();
	void SetX(int x);
	void SetY(int y);
	void Collision(Player jugador, Bullet bala);
	int GetX();
	int GetY();
};

#endif
