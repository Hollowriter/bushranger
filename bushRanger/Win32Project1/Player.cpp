#include "stdafx.h"
#include "Player.h"


Player::Player()
{
	_x = 0;
	_y = 0;
	_health = 100;
	_ammunition = 10;
	_hv = false;
}
void Player::SetX(int x) 
{
	_x = x;
}
void Player::SetY(int y) 
{
	_y = y;
}
void Player::SetHealth(int health) 
{
	_health = health;
}
void Player::SetHv(bool hv) 
{
	_hv = hv;
}
int Player::GetX() 
{
	return _x;
}
int Player::GetY() 
{
	return _y;
}
int Player::GetHealth()
{
	return _health;
}
bool Player::GetHv() 
{
	return _hv;
}
void Player::Movement(char press)
{
	if (press == 'w')
	{
		_x -= 1;
		_hv = true;
	}
	if (press == 's')
	{
		_x += 1;
		_hv = true;
	}
	if (press == 'd')
	{
		_y += 1;
		_hv = false;
	}
	if (press == 'a')
	{
		_y -= 1;
		_hv = false;
	}
}
void Player::Shooting(char shoot) 
{
	if (shoot == 'j')
		_ammunition -= 1;
}
void Player::Reload(char reload) 
{
	if (reload == 'k')
		_ammunition = 10;
}

Player::~Player()
{
}
