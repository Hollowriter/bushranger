#pragma once
#ifndef BUSHGAME_H
#define BUSHGAME_H
#include<iostream>
#include"../Motorcito/Composite.h"
#include"../Motorcito/Camara.h"
#include"../Motorcito/Inputs.h"
#include"../Motorcito/Animador.h"
#include"../Motorcito/BoundingBox.h"
#include"../Motorcito/Conector.h"
#include"../Motorcito/Tiler.h"
#include"../Motorcito/TileRenderer.h"
#include"../Motorcito/Jueguito.h"
#include"../Motorcito/Material.h"
#include"../Motorcito/Modelo.h"
#include"../Motorcito/SeinTexture.h"
#include"Player.h"
#include"Enemy.h"
#include"Wall.h"
#include"Bullet.h"
class BushGame : public Conector
{
private:
	Composite* seinCamera;
	Camara* thyCameraView;
	Inputs* thyKeyboard;
	Player* thyPlayer;
	Meshy* playerMesh;
	Animador* playerMeshRenderer;
	SeinTexture* playerTexture;
	/*Enemy* seinEnemy;
	Meshy* enemyMesh;
	Animador* enemyMeshRenderer;*/
	bool gameStarted;
	bool death;
public:
	LPDIRECT3DDEVICE9 deviant;
	BushGame(/*LPDIRECT3DDEVICE9 devianto*/);
	~BushGame();
	void Jugando();
	void LoadContent();
	void ThyFrames();
};

#endif
