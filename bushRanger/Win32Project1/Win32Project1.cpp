#pragma once
#include "stdafx.h"
#include"Win32Project1.h"

int APIENTRY _tWinMain(_In_     HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_     LPTSTR    lpCmdLine,
	_In_     int       nCmdShow)
{
	Jueguito* cosa = Jueguito::GetInstance();
	BushGame* superJuego = new BushGame();
	cosa->Run(hInstance, nCmdShow, superJuego);
	delete cosa;
	delete superJuego;
}