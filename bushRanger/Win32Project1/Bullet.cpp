#include "stdafx.h"
#include "Bullet.h"


Bullet::Bullet()
{
	_x = 0;
	_y = 0;
	_shoot = false;
	_timer = 0;
}
void Bullet::Move() 
{
	for (int i = 0; i <= _timer; i++) 
	{
	}
}
void Bullet::SetX(int x)
{
	_x = x;
}
void Bullet::SetY(int y)
{
	_y = y;
}
void Bullet::SetShoot(bool shoot)
{
	_shoot = shoot;
}
void Bullet::SetTimer(int timer)
{
	_timer = timer;
}
int Bullet::GetX()
{
	return _x;
}
int Bullet::GetY()
{
	return _y;
}
int Bullet::GetShoot()
{
	return _shoot;
}
int Bullet::GetTimer()
{
	return _timer;
}


Bullet::~Bullet()
{
}
