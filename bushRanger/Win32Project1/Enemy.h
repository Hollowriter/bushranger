#pragma once
#ifndef ENEMY_H
#define ENEMY_H
#include<iostream>
#include"../Motorcito/Composite.h"
class Enemy : public Composite
{
private:
	int _x;
	int _y;
	bool _shooter;
	bool _hv;	
	int _health;
public:
	Enemy();
	Enemy(int x, int y, bool shooter, bool hv, int health);
	~Enemy();
	void SetX(int x);
	void SetY(int y);
	void SetHealthEnemy(int health);
	void SetHv(bool hv);
	void SetShooter(bool shooting);
	void MoveAndShoot();
	int GetX();
	int GetY();
	bool GetShooter();
	int GetHealthEnemy();
	bool GetHv();
};

#endif
