#include "stdafx.h"
#include "BushGame.h"


BushGame::BushGame(/*LPDIRECT3DDEVICE9 devianto*/)
{
	Jueguito* j = Jueguito::GetInstance();
	deviant = j->GetDevice();
}


BushGame::~BushGame()
{
	/*delete enemyMeshRenderer;
	delete enemyMesh;
	delete seinEnemy;*/
	delete playerMeshRenderer;
	delete playerMesh;
	delete playerTexture;
	delete thyPlayer;
	delete thyKeyboard;
	delete thyCameraView;
	delete seinCamera;
}

void BushGame::Jugando()
{
}

void BushGame::LoadContent() 
{
	// creation and initialization
	seinCamera = new Composite();
	thyCameraView = new Camara();
	thyKeyboard = new Inputs();
	thyPlayer = new Player();
	playerMesh = new Meshy();
	playerTexture = new SeinTexture(L"walk.png");
	playerMeshRenderer = new Animador(playerTexture);
	/*seinEnemy = new Enemy();
	enemyMesh = new Meshy();
	enemyMeshRenderer = new Animador();*/
	gameStarted = false;
	death = false;

	seinCamera->Add(thyCameraView);
	thyKeyboard->InputInitializer();
	/*seinCamera->Move(D3DXVECTOR3(1, 1, -10), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(1, 1, 1));*/ // descomentar

	//playerMeshRenderer->SetTexturizationThree(playerTexture);
	playerMeshRenderer->SetMesh(playerMesh);
	thyPlayer->SetModelScale(3, 1, 0);
	thyPlayer->SetVectorPosition(D3DXVECTOR3(1, 0, 1));
	//thyPlayer->Move(D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(1, 1, 1), D3DXVECTOR3(1, 1, 1));
	thyPlayer->Add(playerMeshRenderer);
	playerMeshRenderer->AddAnimationFrame(4, 1);
	playerMeshRenderer->BlendingThing(1);
	/*playerMeshRenderer->WrappingThing(1);
	playerMeshRenderer->FilterThing(1);*/
}

void BushGame::ThyFrames() 
{
	seinCamera->Update();
	seinCamera->Rendering();
	thyPlayer->SetVectorPosition(D3DXVECTOR3(1, 0, 1));
	playerMeshRenderer->UpdateAnimation(0.1);
	thyPlayer->Rendering();
}