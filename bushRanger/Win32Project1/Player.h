#pragma once
#ifndef PLAYER_H
#define PLAYER_H
#include<iostream>
#include "../Motorcito/Composite.h"
class Player : public Composite 
{
private:
		int _x;
		int _y;
		int _health;
		int _ammunition;
		bool _hv;
public:
		Player();
		~Player();
		void Shooting(char shoot);
		void Reload(char reload);
		void Movement(char press);
		void SetX(int x);
		void SetY(int y);
		void SetHealth(int health);
		void SetHv(bool hv);
		int GetX();
		int GetY();
		int GetHealth();
		bool GetHv();
};

#endif
